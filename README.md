# Pritunl-Pi Docker build tools

This project aims on making the deployment of Pritunl servers as easy as possible - even on the Raspberry Pi.

The Pritunl-Pi Docker build tools are downloading the latest version of Go and the specified Pritunl version.
In the next step the Pritunl software is built from source.
The image is called "pritunl-pi" with the tag beeing the version of the Pritunl software.

## Build - Usage:
./build.sh \<Pritunl version\>

## Build - Result:
The command "docker image ls" should now show the following:

    REPOSITORY                                TAG                   IMAGE ID       CREATED          SIZE
    ...
    pritunl-pi                                1.29.2664.67          e4b5d1f72106   13 minutes ago   1.02GB
    ...
