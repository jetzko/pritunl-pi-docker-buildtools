#!/bin/bash


##### Raspberry Pi Pritunl Docker build tools | v0.0.3 | by Jesco Christ #####


set -e

[ -d /dev/net ] ||
  /usr/bin/mkdir -p /dev/net

[ -c /dev/net/tun ] ||
  /usr/bin/mknod /dev/net/tun c 10 200

if ! [ -s /data/pritunl.conf ]; then
    /usr/bin/echo "[entrypoint.sh] Configuration file /data/pritunl.conf is empty or has not yet been created ..."
    /usr/bin/echo "[entrypoint.sh] Creating initial configuration file /data/pritunl.conf ..."
    /usr/bin/cat << EOF > /data/pritunl.conf
{
    "mongodb_uri": "$MONGODB_URI",
    "log_path": "/data/pritunl.log",
    "static_cache": true,
    "temp_path": "/tmp/pritunl_%r",
    "bind_addr": "0.0.0.0",
    "www_path": "/usr/share/pritunl/www",
    "local_address_interface": "auto",
    "port": 443
}
EOF
    /usr/bin/chmod 664 /data/pritunl.conf
    /usr/bin/cat /data/pritunl.conf
else
    /usr/bin/echo "[entrypoint.sh] Configuration file /data/pritunl.conf is already existing ..."
    /usr/bin/cat /data/pritunl.conf
fi

/usr/bin/echo "[entrypoint.sh] Executing \"exec /usr/local/bin/pritunl start\" ..."
exec /usr/local/bin/pritunl start
