#!/bin/bash


##### Raspberry Pi Pritunl Docker build tools | v0.0.1 | by Jesco Christ #####


# Parse command line arguments
PRITUNLVERSION="${1}"

# Check PRITUNLVERSION variable
if [ -z "$PRITUNLVERSION" ]; then
    /usr/bin/echo [ ERROR ] Please specify Pritunl version as first parameter \(e.g. 1.29.2664.67\)... Aborting...
    exit 1
fi


# Change directory
cd "${0%/*}"/image

# Look for errors while changing the directory
if [ $? != 0 ]; then
    /usr/bin/echo [ ERROR ] Unable to enter "${0%/*}"/image subdirectory... Aborting...
    exit 1
fi


# Build docker image
/usr/bin/docker build -t pritunl-pi:"$PRITUNLVERSION" --build-arg PRITUNLVERSION="$PRITUNLVERSION" .

# Look for errors while building docker image
if [ $? != 0 ]; then
    /usr/bin/echo [ ERROR ] Unable to build docker image for Pritunl version "$PRITUNLVERSION"... Aborting...
    exit 1
fi


# Exit without error
exit 0
